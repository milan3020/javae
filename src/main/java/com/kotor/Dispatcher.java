package com.kotor;

import com.kotor.admin.AdminController;
import com.kotor.annotations.Controller;
import com.kotor.annotations.GetMapping;
import com.kotor.annotations.PostMapping;
import com.kotor.logic.ModelView;
import com.kotor.logic.TempName;
import com.kotor.users.HelloController;
import com.kotor.users.LoginController;
import com.kotor.users.MongoController;
import com.kotor.users.RegistrationController;

import javax.servlet.*;
import javax.servlet.annotation.WebFilter;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.File;
import java.io.IOException;
import java.lang.reflect.Constructor;
import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;
import java.net.URL;
import java.util.*;


@WebFilter("/*")
public class Dispatcher implements Filter {
    private final String ADD = "add";
    private final String REGISTRATION = "registration";
    private final String DB = "db";
    private final String MONGO = "mongo";
    private final String ADMIN = "admin";

    private final String PREFIX = "/javae_war";
    private final String TYPE = ".jsp";

    private RegistrationController addController = new RegistrationController();
    private HelloController helloController = new HelloController();
    private AdminController adminController = new AdminController();
    private MongoController mongoController = new MongoController();
    private LoginController loginController = new LoginController();

    private Map<TempName, Method> paths = new HashMap();

    @Override
    public void init(FilterConfig filterConfig) throws ServletException {
        Class[] classes = new Class[0];
        try {
            classes = getClasses("com.kotor");
        } catch (ClassNotFoundException | IOException e) {
            e.printStackTrace();
        }
        for (int i = 0; i < classes.length; i++) {
            Controller classAnnotation = null;
            try {
                classAnnotation = (Controller) classes[i].getAnnotation(Controller.class);
            } catch (Exception e) {
                e.printStackTrace();
            }
            if (classAnnotation != null) {
                Method[] methods = classes[i].getMethods();
                for (int k = 0; k < methods.length; k++) {
                    GetMapping getMapping;
                    PostMapping postMapping;
                    if(methods[k].isAnnotationPresent(GetMapping.class)) {
                        getMapping = methods[k].getAnnotation(GetMapping.class);
                        paths.put(new TempName(classAnnotation.value() + getMapping.value(), "GET"), methods[k]);
                    }
                    if (methods[k].isAnnotationPresent(PostMapping.class)){
                        postMapping = methods[k].getAnnotation(PostMapping.class);
                        paths.put(new TempName(classAnnotation.value() + postMapping.value(), "POST"), methods[k]);
                    }
                }
            }

        }
    }

    @Override
    public void doFilter(ServletRequest request, ServletResponse response, FilterChain chain) throws IOException, ServletException {
//        if(request.getAttribute("modelView") != null){
//            return;
//        }

        if(!(request instanceof HttpServletRequest)) {
            chain.doFilter(request, response);
            return;
        }
        HttpServletRequest httpServletRequest = (HttpServletRequest) request;
        HttpServletResponse httpServletResponse = (HttpServletResponse) response;
        response.setContentType("text/html");
        Map<String, Object> realMap = new HashMap<>();
        Enumeration<String> parameterNames = request.getParameterNames();
        while(parameterNames.hasMoreElements()) {
            String temp = parameterNames.nextElement();
            realMap.put(temp, request.getParameter(temp));
        }
        System.out.println("--------------------------------------------");
        String requestMethod = httpServletRequest.getMethod();
        String path = httpServletRequest.getServletPath();
        for (Map.Entry<String, Object> item: realMap.entrySet()) {
            System.out.println(item.getKey() + " ! " + item.getValue());
        }
        ModelView modelView;
        for (Map.Entry<TempName, Method> item : paths.entrySet()) {
            TempName myRequest = new TempName(path, requestMethod);
            if (item.getKey().getMethod().equals(myRequest.getMethod()) && item.getKey().getURL().equals(myRequest.getURL())) {
                Method method = item.getValue();
                try {
                    Class myClass = method.getDeclaringClass();
                    Class<?> clazz = Class.forName(myClass.getName());
                    Constructor<?> constructor = clazz.getConstructor();
                    Object object = constructor.newInstance(new Object[]{});

                    modelView = (ModelView) method.invoke(object, realMap);
                    request.setAttribute("modelView", modelView);
                    httpServletRequest.getRequestDispatcher( modelView.getTemplate() + TYPE)
                            .forward(httpServletRequest, httpServletResponse);

                } catch (IllegalAccessException e) {
                    e.printStackTrace();
                } catch (InvocationTargetException e) {
                    e.printStackTrace();
                } catch (NoSuchMethodException e) {
                    e.printStackTrace();
                } catch (InstantiationException e) {
                    e.printStackTrace();
                } catch (ClassNotFoundException e) {
                    e.printStackTrace();
                }
            }
        }
    }

    private static Class[] getClasses(String packageName)
            throws ClassNotFoundException, IOException {
        ClassLoader classLoader = Thread.currentThread().getContextClassLoader();
        assert classLoader != null;
        String path = packageName.replace('.', '/');
        Enumeration<URL> resources = classLoader.getResources(path);
        List<File> dirs = new ArrayList<File>();
        while (resources.hasMoreElements()) {
            URL resource = resources.nextElement();
            dirs.add(new File(resource.getFile()));
        }
        ArrayList<Class> classes = new ArrayList<Class>();
        for (File directory : dirs) {
            classes.addAll(findClasses(directory, packageName));
        }
        return classes.toArray(new Class[classes.size()]);
    }
    private static List<Class> findClasses(File directory, String packageName) throws ClassNotFoundException {
        List<Class> classes = new ArrayList<Class>();
        if (!directory.exists()) {
            return classes;
        }
        File[] files = directory.listFiles();
        for (File file : files) {
            if (file.isDirectory()) {
                assert !file.getName().contains(".");
                classes.addAll(findClasses(file, packageName + "." + file.getName()));
            } else if (file.getName().endsWith(".class")) {
                classes.add(Class.forName(packageName + '.' + file.getName().substring(0, file.getName().length() - 6)));
            }
        }
        return classes;
    }
}
