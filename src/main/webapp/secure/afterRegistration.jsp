<%@ page import="com.kotor.logic.ModelView" %><%--
  Created by IntelliJ IDEA.
  User: Kaseya
  Date: 30.08.2019
  Time: 14:16
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<html>
<head>
    <title>Status if registration</title>
</head>
<body>
    <%
        ModelView modelView = (ModelView) request.getAttribute("modelView");
        String message = modelView.getMap().get("message").toString();

    %>
    <p><%=message%></p>
    <p>Now you may log in! <a href="login.jsp">Enter</a> </p>
</body>
</html>
