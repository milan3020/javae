package com.kotor.users;

import com.kotor.annotations.Controller;
import com.kotor.annotations.GetMapping;
import com.kotor.logic.ModelView;

import javax.validation.constraints.NotNull;
import java.util.HashMap;
import java.util.Map;

@Controller("/greeting")
public class HelloController {
    @GetMapping("/hello")
    public ModelView doGet(@NotNull HashMap<String, Object> map) {
        String name = map.get("name").toString();
        ModelView modelView = new ModelView();
        modelView.setTemplate("/greeting/hello");
        Map<String, Object>  customMap = modelView.getMap();
        customMap.put("name", name);
        System.out.println("No mistake");
        return modelView;
    }

}
