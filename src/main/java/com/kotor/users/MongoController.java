package com.kotor.users;

import com.kotor.annotations.Controller;
import com.kotor.annotations.GetMapping;
import com.kotor.dao.Mongo;
import com.kotor.dao.User;
import com.mongodb.Block;
import com.mongodb.client.MongoCollection;
import com.mongodb.client.MongoDatabase;
import org.bson.Document;

import java.io.PrintWriter;
import java.util.Map;

@Controller("/db")
public class MongoController {
    @GetMapping("/list")
    public void doGet(Map<String, Object> map) {
        Mongo mongo = new Mongo();
        MongoDatabase mongoDatabase = mongo.createConnection();
        User firstUser = new User("Michael", "Kotor", 18, "milan3020@gmail.com", "A1234567b");
        try {
            mongoDatabase.getCollection("User");
        } catch (Exception e) {
            e.printStackTrace();
        }
        for (String name : mongoDatabase.listCollectionNames()) {
            //printWriter.println("<h4>" + name + "</h4>");
        }
        MongoCollection collection = mongoDatabase.getCollection("User");
        Document document = new Document();
        document.append("firstName", "Michael");
        document.append("lastName", "Kotor");
        document.append("age", 18);
        document.append("email", "milan3020@gmail.com");
        document.append("password", "A1234567b");
        collection.insertOne(document);
        //printWriter.println("FirstUser was inserted!");

        Block<Document> printBlock = document1 -> //printWriter.println(document1.toJson());
        map.put("user", firstUser);
    }
}
