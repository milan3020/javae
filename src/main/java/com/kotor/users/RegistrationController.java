package com.kotor.users;

import com.kotor.annotations.Controller;
import com.kotor.annotations.GetMapping;
import com.kotor.annotations.PostMapping;
import com.kotor.dao.Mongo;
import com.kotor.logic.ModelView;
import com.mongodb.client.MongoCollection;
import com.mongodb.client.MongoDatabase;
import org.bson.Document;

import java.io.PrintWriter;
import java.util.Map;

@Controller("/secure")
public class RegistrationController {
    private final Mongo mongo = new Mongo();
    private final MongoDatabase mongoDatabase = mongo.getDatabase();
    private final MongoCollection mongoCollection = mongoDatabase.getCollection("User");

    private final String firstName = "firstName";
    private final String lastName = "lastName";
    private final String age = "age";
    private final String email = "email";
    private final String password = "password";

    public RegistrationController() {}

    @PostMapping("/registration")
    public ModelView doPost(Map<String, Object> map) {
        ModelView modelView = new ModelView();
        modelView.setTemplate("/secure/afterRegistration");
        Map<String, Object> customMap = modelView.getMap();
        try {
            Document document = new Document();
            document.append(firstName, map.get(firstName))
                    .append(lastName, map.get(lastName))
                    .append(age, map.get(age))
                    .append(email, map.get(email))
                    .append(password, map.get(password));
            mongoCollection.insertOne(document);
            customMap.put("message", "Registration passed well!");
        } catch (Exception e) {
            customMap.put("message", "There was an Error!");
        } finally {
            return modelView;
        }
    }

    @GetMapping("/registration")
    public ModelView doGet(Map<String, Object> map)  {
        ModelView modelView = new ModelView();
        modelView.setTemplate("/secure/registration");
        return modelView;
    }
}
