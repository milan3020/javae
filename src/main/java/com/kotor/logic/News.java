package com.kotor.logic;

import java.util.ArrayList;
import java.util.Date;

public class News {
    private String name;
    private Date date;
    private String content;
    private String author;
    private ArrayList<Comment> comments;

    public final static String NAME = "name";
    public final static String DATE = "date";
    public final static String CONTENT = "content";
    public final static String AUTHOR = "author";
    public final static String COMMENTS = "comments";

    public News(String name, Date date, String content, String author, ArrayList<Comment> comments) {
        this.name = name;
        this.date = date;
        this.content = content;
        this.author = author;
        this.comments = comments;
    }
}
