package com.kotor.logic;

public class TempName {
    private String URL;
    private String method;

    public TempName(String URL, String method) {
        this.URL = URL;
        this.method = method;
    }

    public String getURL() {
        return URL;
    }

    public void setURL(String URL) {
        this.URL = URL;
    }

    public String getMethod() {
        return method;
    }

    public void setMethod(String method) {
        this.method = method;
    }

    @Override
    public String toString() {
        return "TempName{" +
                "URL='" + URL + '\'' +
                ", method='" + method + '\'' +
                '}';
    }
}
