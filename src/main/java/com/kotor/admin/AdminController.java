package com.kotor.admin;

import com.kotor.annotations.Controller;
import com.kotor.annotations.GetMapping;
import com.kotor.dao.Mongo;
import com.mongodb.Block;
import com.mongodb.client.MongoCollection;
import com.mongodb.client.MongoDatabase;
import org.bson.Document;

import java.io.PrintWriter;
import java.util.Map;

@Controller("/admin")
public class AdminController {
    private final Mongo mongo = new Mongo();
    private final MongoDatabase mongoDatabase = mongo.getDatabase();
    private final MongoCollection mongoCollection = mongoDatabase.getCollection("User");

    @GetMapping("/list")
    public void doGet(Map<String, Object> map, final PrintWriter printWriter) {
        Block<Document> printBlock = document -> printWriter.println("<p>" + document.toJson()  +"</p>");
        mongoCollection.find().forEach(printBlock);
    }
}
