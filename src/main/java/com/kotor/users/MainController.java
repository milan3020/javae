package com.kotor.users;

import com.kotor.annotations.Controller;
import com.kotor.annotations.GetMapping;
import com.kotor.dao.Mongo;
import com.kotor.logic.ModelView;
import com.kotor.logic.News;
import com.mongodb.Block;
import com.mongodb.DBCursor;
import com.mongodb.DBObject;
import com.mongodb.client.FindIterable;
import com.mongodb.client.MongoCollection;
import com.mongodb.client.MongoCursor;
import com.mongodb.client.MongoDatabase;
import org.bson.Document;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import static com.kotor.logic.News.*;

@Controller("/content")
public class MainController {
    @GetMapping("/news")
    public ModelView doGet(Map<String, Object> map) {
        boolean isAuth = (boolean) map.get("isAuth");
        ModelView modelView = new ModelView();
        Map<String, Object> customMap = modelView.getMap();
        if (!isAuth) {
            modelView.setTemplate("/secure/login");
            customMap.put("isAuth", false);
            return modelView;
        } else {
            modelView.setTemplate("/content/news");
            customMap.put("isAuth", true);
            Mongo mongo = new Mongo();
            MongoDatabase mongoDatabase = mongo.createConnection();
            MongoCollection mongoCollection = mongoDatabase.getCollection("news");
            ArrayList<Document> newsList = new ArrayList();

            FindIterable<Document> documents = mongoCollection.find();

            // preparing for an iteration
            MongoCursor<Document> cursor = documents.iterator();
            ArrayList<News> news = new ArrayList();
            while(cursor.hasNext()) {
                Document document = cursor.next();

            }
            return modelView;
        }
    }
}
