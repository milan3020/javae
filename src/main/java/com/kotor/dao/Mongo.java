package com.kotor.dao;

import com.mongodb.MongoClient;
import com.mongodb.MongoClientSettings;
import com.mongodb.client.MongoCollection;
import com.mongodb.client.MongoDatabase;
import org.bson.codecs.configuration.CodecRegistry;
import org.bson.codecs.pojo.PojoCodecProvider;

import javax.jws.soap.SOAPBinding;

import static org.bson.codecs.configuration.CodecRegistries.fromProviders;
import static org.bson.codecs.configuration.CodecRegistries.fromRegistries;


public class Mongo {
    private MongoDatabase mongoDatabase;
    private final MongoClient mongoClient;

    private final String HOST = "localhost";
    private final int PORT = 27017;

    public MongoDatabase createConnection() {
        MongoClient mongoClient= new MongoClient("localhost", 27017);
        MongoDatabase mongoDatabase = mongoClient.getDatabase("michael");
        return mongoDatabase;
    }

    public Mongo() {
        mongoClient = new MongoClient(HOST, PORT);
    }

    public MongoDatabase getDatabase() {
        mongoDatabase = mongoClient.getDatabase("michael");
        return mongoDatabase;
    }

    public MongoDatabase getDatabase(String name) {
        MongoDatabase mongoDatabase = mongoClient.getDatabase(name);
        return mongoDatabase;
    }
}

