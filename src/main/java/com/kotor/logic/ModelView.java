package com.kotor.logic;

import java.util.HashMap;
import java.util.Map;

public class ModelView {
    private String template;
    private Map<String, Object> map = new HashMap<>();

    public ModelView() {}

    public ModelView(String template, Map<String, Object> map) {
        this.template = template;
        this.map = map;
    }

    public String getTemplate() {
        return template;
    }

    public void setTemplate(String template) {
        this.template = template;
    }

    public Map<String, Object> getMap() {
        return map;
    }

    public void setMap(Map<String, Object> map) {
        this.map = map;
    }
}
