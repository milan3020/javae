<%@ page import="com.kotor.logic.ModelView" %>
<%@ page import="java.io.PrintWriter" %><%--
  Created by IntelliJ IDEA.
  User: Kaseya
  Date: 29.08.2019
  Time: 17:05
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<html>
<head>
    <title>Title</title>
</head>
<body>
   <%
       String name;
       try(PrintWriter printWriter = response.getWriter()) {
           ModelView modelView = (ModelView) request.getAttribute("modelView");
           printWriter.println("<h2> Hello, " + modelView.getMap().get("name") + "!</h2>");
           name = modelView.getMap().get("name").toString();
       }
       %>
    <div>
        You should see something,<%=name%>
    </div>
</body>
</html>
