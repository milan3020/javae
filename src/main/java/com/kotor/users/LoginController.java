package com.kotor.users;

import com.kotor.annotations.Controller;
import com.kotor.annotations.GetMapping;
import com.kotor.annotations.PostMapping;
import com.kotor.dao.Mongo;
import com.kotor.logic.ModelView;
import com.mongodb.client.FindIterable;
import com.mongodb.client.MongoCollection;
import com.mongodb.client.MongoDatabase;

import javax.jws.WebParam;
import java.io.PrintWriter;
import java.util.Map;

import static com.mongodb.client.model.Filters.and;
import static com.mongodb.client.model.Filters.eq;

@Controller("/secure")
public class LoginController {
    private final Mongo mongo = new Mongo();
    private final MongoDatabase mongoDatabase = mongo.getDatabase();
    private final MongoCollection mongoCollection = mongoDatabase.getCollection("User");

    @PostMapping("/login")
    public ModelView doPost(Map<String, Object> map) {
        String login = map.get("email").toString();
        String password = map.get("password").toString();
        ModelView modelView = new ModelView();
        Map<String, Object> customMap = modelView.getMap();
        FindIterable findIterable = mongoCollection.find(and(eq("email", login), eq("password", password)));
        if (findIterable == null) {
            customMap.put("isAuth", false);
        } else {
            customMap.put("isAuth", true);
        }
        modelView.setTemplate("/");
        return modelView;
    }
}
